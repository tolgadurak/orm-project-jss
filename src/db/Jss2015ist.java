package db;

import com.tolga.db.DbContext;
import com.tolga.db.DbTable;

import model.Course;
import model.Grade;
import model.Student;

public class Jss2015ist extends DbContext {
	private DbTable<Student> students;
	private DbTable<Course> courses;
	private DbTable<Grade> grades;

	public DbTable<Student> getStudents() {
		return students;
	}

	public void setStudents(DbTable<Student> students) {
		this.students = students;
	}

	public DbTable<Course> getCourses() {
		return courses;
	}

	public void setCourses(DbTable<Course> courses) {
		this.courses = courses;
	}

	public DbTable<Grade> getGrades() {
		return grades;
	}

	public void setGrades(DbTable<Grade> grades) {
		this.grades = grades;
	}

}
