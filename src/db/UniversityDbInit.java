package db;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.tolga.initializer.DropInitDatabase;

import model.Student;

public class UniversityDbInit extends DropInitDatabase<Jss2015ist> {

	
	public static void main(String[] args) {
		new UniversityDbInit();	
	}
	@Override
	public boolean seed(Jss2015ist t) throws SQLException {
		List<Student> students = new ArrayList<Student>();
		students.add(new Student(1, "Tolga", "Durak", 22,"�stanbul �niversitesi", 2016));
		students.add(new Student(2, "Sel�uk", "Durak", 22,"�stanbul �niversitesi", 2016));
		t.getStudents().addRange(students);
		
		return true;
	}
}
