package com.tolga.initializer;

import java.io.File;
import java.lang.reflect.Field;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.tolga.adapter.DatabaseMappingConst;

public final class ModelMapper {

	private static HashMap<Class<?>, ArrayList<Field>> map;

	private static final char DOT = '.';

	private static final char SLASH = '/';

	private static final String CLASS_SUFFIX = ".class";

	private static final String BAD_PACKAGE_ERROR = "Unable to get resources from path '%s'."
			+ " Are you sure the package '%s' exists?";

	public ModelMapper() throws ClassNotFoundException {

		performMapping();
	}

	private void performMapping() throws ClassNotFoundException {

		if (map == null) {
			map = new HashMap<Class<?>, ArrayList<Field>>();
			for (Class<?> cs : ModelMapper.find("db")) {
				if (cs.getAnnotatedSuperclass().getType().getTypeName()
						.contains(DatabaseMappingConst.DbContext)) {
					for (Field f : cs.getDeclaredFields()) {
						if (f.getType().getName()
								.equals(DatabaseMappingConst.DbTable)) {
							String dbContextField = f.getGenericType()
									.getTypeName();
							dbContextField = dbContextField.substring(
									dbContextField.indexOf("<") + 1,
									dbContextField.indexOf(">"));
							Class<?> modelClass = Class.forName(dbContextField);
							ArrayList<Field> fields = new ArrayList<>();
							for (Field field : modelClass.getDeclaredFields()) {
								fields.add(field);
							}
							for (Field field : modelClass.getSuperclass()
									.getDeclaredFields()) {
								fields.add(field);
							}
							map.put(modelClass, fields);
						}
					}
				}
			}
		}
	}

	public HashMap<Class<?>, ArrayList<Field>> getMappedModel() {
		return map;
	}

	private static List<Class<?>> find(String scannedPackage) {
		String scannedPath = scannedPackage.replace(DOT, SLASH);
		URL scannedUrl = Thread.currentThread().getContextClassLoader()
				.getResource(scannedPath);
		if (scannedUrl == null) {
			throw new IllegalArgumentException(String.format(BAD_PACKAGE_ERROR,
					scannedPath, scannedPackage));
		}
		File scannedDir = new File(scannedUrl.getFile());
		List<Class<?>> classes = new ArrayList<Class<?>>();
		for (File file : scannedDir.listFiles()) {
			classes.addAll(find(file, scannedPackage));
		}
		return classes;
	}

	private static List<Class<?>> find(File file, String scannedPackage) {
		List<Class<?>> classes = new ArrayList<Class<?>>();
		String resource = scannedPackage + DOT + file.getName();
		if (file.isDirectory()) {
			for (File child : file.listFiles()) {
				classes.addAll(find(child, resource));
			}
		} else if (resource.endsWith(CLASS_SUFFIX)) {
			int endIndex = resource.length() - CLASS_SUFFIX.length();
			String className = resource.substring(0, endIndex);
			try {
				classes.add(Class.forName(className));
			} catch (ClassNotFoundException ignore) {
			}
		}
		return classes;
	}

}
