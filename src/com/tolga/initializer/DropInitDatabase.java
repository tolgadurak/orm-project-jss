package com.tolga.initializer;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.tolga.adapter.DatabaseInitAdapter;
import com.tolga.adapter.SqlCommandAdapter;
import com.tolga.connection.MyConnection;

public abstract class DropInitDatabase<T> implements DatabaseInitAdapter<T> {
	private String dbName;
	private Connection conn;
	private PreparedStatement ps;
	private ResultSet rs;

	@SuppressWarnings("unchecked")
	protected DropInitDatabase() {
		// Reflection to get dbName in runtime
		this.dbName = ((Class<T>) ((ParameterizedType) this.getClass()
				.getGenericSuperclass()).getActualTypeArguments()[0])
				.getSimpleName();
		// /////
		try {
			conn = MyConnection.getInstance("com.mysql.jdbc.Driver",
					"jdbc:mysql://localhost/deneme", "root", "");
			init();
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}

	}

	private boolean init() throws SQLException {
		if (!isExists()) {
			return createDatabase();
		} else {
			dropDatabase();
			return createDatabase();
		}
	}

	private boolean isExists() throws SQLException {
		List<String> arrayList = new ArrayList<String>();
		this.ps = conn.prepareStatement(SqlCommandAdapter.SHOW_DATABASES);
		this.rs = ps.executeQuery();
		while (rs.next()) {
			arrayList.add(rs.getString(1));
		}
		return arrayList.contains(this.dbName.toLowerCase()) ? true : false;
	}

	private boolean dropDatabase() throws SQLException {
		boolean result = false;
		this.ps = conn.prepareStatement("DROP DATABASE " + dbName);
		result = ps.execute();
		ps.close();
		return result;
	}

	private boolean createDatabase() throws SQLException {
		boolean result = false;
		this.ps = conn.prepareStatement("CREATE DATABASE " + dbName);
		result = ps.execute();
		ps.close();
		return result;
	}

}
