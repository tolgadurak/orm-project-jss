package com.tolga.adapter;

public interface DatabaseMappingConst {
	String DropInitDatabase = "com.tolga.initializer.DropInitDatabase";
	String DbContext = "com.tolga.db.DbContext";
	String DbQuery = "com.tolga.db.DbQuery";
	String DbTable = "com.tolga.db.DbTable";
}
