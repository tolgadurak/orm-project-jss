package com.tolga.adapter;

import java.sql.SQLException;

public interface DatabaseInitAdapter<T> {
	boolean seed(T t) throws SQLException;
}
