package com.tolga.adapter;

import java.util.List;

public interface SqlCommandAdapter<T> {
	String SHOW_TABLES = "show tables;";
	String SHOW_DATABASES = "show databases;";
	public void add(T model);
	public void addRange(List<T> modelList);
	public void remove(T model);
	public void removeAll();
	public void update(T source, T newRecord);

}
