package com.tolga.db;

import java.util.List;

public class DbTable<T> extends DbQuery<T> {

	private DbTable(T model) {
		super(model);
	}
	@Override
	public void add(T model) {
		super.getAll().add(model);
	}

	@Override
	public void addRange(List<T> modelList) {
		super.getAll().addAll(modelList);
	}

	@Override
	public void remove(T model) {
		super.getAll().remove(model);
	}

	@Override
	public void removeAll() {
		super.getAll().removeAll(super.getAll());
	}

	@Override
	public void update(T source, T newRecord) {
		super.getAll().set(super.getAll().indexOf(source), newRecord);
	}
}
