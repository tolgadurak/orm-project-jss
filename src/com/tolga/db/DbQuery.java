package com.tolga.db;

import java.util.ArrayList;
import java.util.List;

import com.tolga.adapter.SqlCommandAdapter;

public abstract class DbQuery<T> implements SqlCommandAdapter<T> {
	private List<T> model;

	public DbQuery(T obj) {
		this.model = new ArrayList<T>();
	}

	protected List<T> getAll() {
		return this.model;
	}

	protected List<T> getRange(int from, int to) {
		return this.model.subList(from, to);
	}
}
