package com.tolga.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MyConnection {
	private static Connection conn;

	private MyConnection() {
	}

	public static Connection getInstance(String driverName, String url,
			String user, String pass) throws ClassNotFoundException,
			SQLException {
		Class.forName(driverName);
		return conn == null ? conn = DriverManager.getConnection(url, user,
				pass) : conn;
	}
}
