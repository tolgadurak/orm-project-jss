package model;

public class Student extends Person {

	private int studentId;
	private int age;
	private String university;
	private int gradYear;

	public Student(int studentId, String firstName, String lastName, int age,
			String university, int gradYear) {
		super(firstName, lastName);
		this.studentId = studentId;
		this.age = age;
		this.university = university;
		this.gradYear = gradYear;
	}

	// region getter and setters
	public int getStudentId() {
		return studentId;
	}

	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getUniversity() {
		return university;
	}

	public void setUniversity(String university) {
		this.university = university;
	}

	public int getGradYear() {
		return gradYear;
	}

	public void setGradYear(int gradYear) {
		this.gradYear = gradYear;
	}
	// end region
}
